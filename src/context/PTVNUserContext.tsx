import React, { useContext } from 'react';

const PTVNUserContextDataDefault = () => {
  return {
    login: () => { },
    logout: () => { },
    getCurrentUser: () => { }
  }
}

export type PTVNUserContextData = ReturnType<typeof PTVNUserContextDataDefault>;

const PTVNUserContext = React.createContext<PTVNUserContextData>(
  PTVNUserContextDataDefault()
);

const usePTVNUser = () => useContext(PTVNUserContext);

const PTVNUserProvider = ({ children }: React.PropsWithChildren<unknown>) => {
  const ptvnUserContextData = PTVNUserContextDataDefault();

  return (
    <PTVNUserContext.Provider value={ptvnUserContextData}>
      {children}
    </PTVNUserContext.Provider>
  );
};

export { PTVNUserProvider, usePTVNUser };
