export interface LanguageItem {
  key: string;
  title: string;
}
export class Language {
  public static readonly en: LanguageItem = { key: "en", title: "English" };
  public static readonly vn: LanguageItem = { key: "vn", title: "tiếng Việt" };
  public static readonly th: LanguageItem = { key: "th", title: "ภาษาไทย" };

  public static getLangByKey(key: string) {
    switch (key) {
      case Language.en.key:
        return Language.en;
      case Language.vn.key:
        return Language.vn;
      case Language.th.key:
        return Language.th;
      default:
        return undefined;
    }
  }
}
